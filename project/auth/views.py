import os, pdb
from flask import Blueprint, render_template, session, url_for, redirect, request, flash, g
from flask import copy_current_request_context
from functools import wraps
from project.models import User
from project import db


auth_blueprint = Blueprint(
	'auth',
	__name__,
	template_folder='templates'
	)

CONF_URL = 'https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration'
LOGOUT_URL = 'https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout'


from project import app

app.config["OIDC_CLIENT_SECRETS"] = "secrets/client_secrets.json"
app.config["OIDC_COOKIE_SECURE"] = True
app.config["OIDC_CALLBACK_ROUTE"] = "/oidc/callback"
app.config["OIDC_SCOPES"] = ["openid", "email", "profile"]

app.config['USERFILES_PATH'] = '../database/userfiles'

from flask_oidc import OpenIDConnect
oidc = OpenIDConnect(app)
                                                                                    
def login_required(func):
    @wraps(func)
    def inner(*args, **kwargs):
        if g.oidc_id_token:
            user = g.oidc_id_token
            path = os.path.join(app.root_path, app.config['USERFILES_PATH'], user['cern_upn'])
            session['USERFILES_PATH'] = path # path for user files folder
            session['USER'] = user['cern_upn']
            session['USER_NAME'] = user['name']
            return func(*args, **kwargs)
        else:
            session['LAST_REQUESTED_URL'] = request.path
            return redirect(url_for('auth.login'))
    return inner

def keep_unique_session(func):
    @wraps(func)
    def inner(*args, **kwargs):
        user = g.oidc_id_token
        if not user: return func(*args, **kwargs)
        currentToken = user['session_state']
        myquery = User.query.filter_by(username=user['cern_upn']).first()
        token = myquery.session_token
        if token != currentToken:
            oidc.logout()
            flash('You are logged out due to recent log-in at another place!')
            return redirect("{}?redirect_uri={}".format(
                LOGOUT_URL,
                url_for('auth.index', _external=True))
            )
        return func(*args, **kwargs)
    return inner

@auth_blueprint.route('/')
def index():
    user = g.oidc_id_token
    session['LAST_REQUESTED_URL'] = request.path    
    return render_template('auth/index.html', user_name=user['name'] if user else None)

@auth_blueprint.route('/login')
@oidc.require_login
def login():
    user = g.oidc_id_token
    if not save_user(user): flash('Welcome {}!!!'.format(user['name']))
    return redirect(url_for('auth.authorize'))

@auth_blueprint.route('/authorize')
def authorize():
    user = g.oidc_id_token #oidc.user_getinfo()
    path = os.path.join(app.root_path, app.config['USERFILES_PATH'], user['cern_upn'])
    os.makedirs(path, exist_ok = True)
    print(session)
    return redirect(session['LAST_REQUESTED_URL'])

@auth_blueprint.route('/logout')
def logout():
    print('logged out.', session)
    oidc.logout()
    flash('Successfully logged out!')
    return redirect("{}?redirect_uri={}".format(
        LOGOUT_URL,
        url_for('auth.index', _external=True))
    )

@auth_blueprint.route('/userinfo')
@login_required
def userinfo():
    print(request.remote_addr)
    return g.oidc_id_token

def save_user(user):
    query = User.query.filter_by(username=user['cern_upn']).first()
    if query:
        query.session_token = user['session_state']
        db.session.commit()
        return True
    else:
        query = User(username=user['cern_upn'], email=user['email'], session_token=user['session_state'])
        db.session.add(query)
        db.session.commit()
        return False
