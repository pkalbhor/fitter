from project import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    login_count = db.Column(db.Integer, unique=False, nullable=True)
    session_token = db.Column(db.String(40), index=True)

    def __repr__(self):
        return '<User %r>' % self.username
