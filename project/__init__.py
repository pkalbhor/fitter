import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../database/userbase.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = os.urandom(24)
db = SQLAlchemy(app)

from project.auth.views import auth_blueprint
app.register_blueprint(auth_blueprint)

@app.route('/')
def index():
	return redirect(url_for('auth.index'))
