from project import app
from project import db
db.create_all()

if __name__ == '__main__':
    # serve(app, host='0.0.0.0', port=8080)
    app.run(debug=True, port=5000)
